import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './style.css';

import validation from '../helpers/validation';

class CommentForm extends Component {
  constructor(props) {
    super(props);
    const comment = this.props.comment;
    const type = this.props.type; 
    this.state = { comment, type };
    
    this.updateComment = this.updateComment.bind(this);
    this.removeComment = this.removeComment.bind(this);
    this.createComment = this.createComment.bind(this);
  }

  createComment() {
    const comment = { 
      email: this.detailsEmail.value,
      name: this.detailsName.value,
      body: this.detailsBody.value,
    };

    if (validation.isEmail(comment.email)) {
      this.props.createComment(comment);
      this.detailsEmail.value = '';
      this.detailsName.value = '';
      this.detailsBody.value = '';
    } else {
      alert('Dodaj email');
    }
  }

  updateComment() {
    const comment = { 
      email: this.detailsEmail.value,
      name: this.detailsName.value,
      body: this.detailsBody.value,
    };

    if (validation.isEmail(comment.email)) {
      this.props.updateComment(comment);
    } else {
      alert('Dodaj email');
    }
  }

  removeComment() {
    this.props.removeComment();
  }

  renderButtons() {
    const type = this.state.type;
    let actions;

    if (type === 'edit') {
      actions = (
        <footer className="comment-details__actions comment-details__group">
          <Link className="btn--cancel btn" to="/">Cancel</Link>
          <button className="btn--update btn" onClick={this.updateComment}>Update comment</button>
          <button className="btn--delete btn" onClick={this.removeComment}>Delete</button>
        </footer>
      );
    } else {
      actions = (
        <footer className="comment-details__actions comment-details__group">
          <Link className="btn--cancel btn" to="/">Cancel</Link>
          <button className="btn--update btn" onClick={this.createComment}>Create comment</button>
        </footer>
      );     
    }

    return actions;
  }

  render() {
    const comment = this.state.comment;

    return (
      <div className="comment-form__content">
        <div className="comment-form__group">
          <label className="comment-form__label">E-mail</label>
          <input 
            className="comment-form__input" 
            defaultValue={comment.email} 
            ref={(input) => { this.detailsEmail = input; }}
            type="text" 
          />
        </div>

        <div className="comment-form__group">
          <label className="comment-form__label">Name</label>
          <textarea 
            className="comment-form__input--textarea comment-form__input" 
            defaultValue={comment.name} 
            ref={(input) => { this.detailsName = input; }}
            type="text" 
          />
        </div>
        
        <div className="comment-form__group">
          <label className="comment-form__label">Body</label>
          <textarea 
            className="comment-form__input--textarea comment-form__input" 
            defaultValue={comment.body} 
            ref={(input) => { this.detailsBody = input; }}
            type="text" 
          />
        </div>

        {this.renderButtons()}
      </div>
    );
  }
}

CommentForm.propTypes = {
  comment: PropTypes.object,
  type: PropTypes.string,
  createComment: PropTypes.func,
  updateComment: PropTypes.func,
  removeComment: PropTypes.func,
};

CommentForm.defaultProps = {
  comment: { email: '', name: '', body: '' },
  type: 'add',
  createComment: () => {},
  updateComment: () => {},
  removeComment: () => {},
};


export default CommentForm;
