Wysyłam póki co tylko crud, z powodu wyjazdu urlopowego stornę wyślę do jutro. W razie jakiegoś kontaktu to od środy do niedzieli będę niedostępny ;)

Live crud dostępny pod:
https://simple-crud-904fa.firebaseapp.com/

####Odwzoruj w jak najdokładniejszy sposób zaprezentowaną stronę(img/site.png).

Dla chętnych:

* Dodatkowo dodaj elementy RWD
* MobileFirst :]
* Bem



###Stwórz CRUD’a dzięki któremu będzie można zarządzać zasobami: 
```http://jsonplaceholder.typicode.com/comments```

###Feature’y:  tworzenie/edycja/listowanie/usuwanie

* Dodatkowo możesz dodać paginację ```http://jsonplaceholder.typicode.com/comments?_page=7&_limit=2```
* Dodatkowo możesz dodać filtrowanie po wybranych kolumnach
